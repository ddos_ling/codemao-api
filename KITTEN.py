import requests
import json
import time

def time_conversion(timeStamp):
    timeArray = time.localtime(timeStamp)
    otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
    return(otherStyleTime)

id=input("KITTEN ID:")
get_url='https://api.codemao.cn/api/work/info/'+id
yang=requests.get(get_url)
json_name='KITTEN-'+id+'.json'
result=open(json_name,'w')
result.write(yang.text)
result.close()
open_json=open(json_name,'r',encoding='cp936')
KITTEN_json=json.load(open_json)
open_json.close()

print("作品信息如下：")
print("作品名称：",KITTEN_json["data"]["workDetail"]["workInfo"]["name"])
print("作品ID：",KITTEN_json["data"]["workDetail"]["workInfo"]["id"])
print("作品介绍：",KITTEN_json["data"]["workDetail"]["workInfo"]["description"])
print("作品流量：",KITTEN_json["data"]["workDetail"]["workInfo"]["view_times"])
print("作品再创造/购买量：",KITTEN_json["data"]["workDetail"]["workInfo"]["fork_times"])
print("作品点赞量：",KITTEN_json["data"]["workDetail"]["workInfo"]["praise_times"])
print("作品收藏量：",KITTEN_json["data"]["workDetail"]["workInfo"]["collection_times"])
print("作品创建时间：",time_conversion(KITTEN_json["data"]["workDetail"]["workInfo"]["create_time"]))
print("作品更新时间：",time_conversion(KITTEN_json["data"]["workDetail"]["workInfo"]["publish_time"]))
print("作品是否开源：",KITTEN_json["data"]["workDetail"]["workInfo"]["isallow_fork"])
print("作者信息如下：")
print("作者昵称：",KITTEN_json["data"]["workDetail"]["userInfo"]["nickname"])
print("作者ID：",KITTEN_json["data"]["workDetail"]["userInfo"]["id"])
print("作者个性签名：",KITTEN_json["data"]["workDetail"]["userInfo"]["description"])